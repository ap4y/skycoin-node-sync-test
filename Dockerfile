FROM golang:latest

RUN go get -v github.com/skycoin/skycoin/...

WORKDIR /go/src/skycoin-node-sync-test
COPY . .

RUN go get -v ./...
RUN go install -v ./...

CMD ["skycoin-node-sync-test", "skycoin"]
