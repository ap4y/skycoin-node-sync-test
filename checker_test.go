package main

import (
	"fmt"
	"testing"
	"time"

	"github.com/skycoin/skycoin/src/api"
	"github.com/skycoin/skycoin/src/readable"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCheckNodeConnWaitFailed(t *testing.T) {
	mockClient := &MockClient{networkConnectionErr: fmt.Errorf("network failure")}
	checker := &Checker{mockClient, 1, 600 * time.Millisecond}

	res := checker.CheckNode(3, time.Second)
	assert.False(t, res.Success)

	require.NotNil(t, res.Error)
	assert.Equal(t, "failed to reach 3 active connections: context deadline exceeded", res.Error.Error())
}

func TestCheckNodeConnWaitTimeout(t *testing.T) {
	mockClient := &MockClient{connNum: 2}
	checker := &Checker{mockClient, 1, 600 * time.Millisecond}

	res := checker.CheckNode(3, time.Second)
	assert.False(t, res.Success)

	require.NotNil(t, res.Error)
	assert.Equal(t, "failed to reach 3 active connections: context deadline exceeded", res.Error.Error())
}

func TestCheckNodeBlockWaitFailed(t *testing.T) {
	mockClient := &MockClient{connNum: 3, blockchainProgressErr: fmt.Errorf("network failure")}
	checker := &Checker{mockClient, 1, 600 * time.Millisecond}

	res := checker.CheckNode(3, time.Second)
	assert.False(t, res.Success)

	require.NotNil(t, res.Error)
	assert.Equal(t, "sync process failed: number of attempts exceeded", res.Error.Error())
}

func TestCheckNodeBlockWaitTimeout(t *testing.T) {
	mockClient := &MockClient{connNum: 3, currentBlock: 1, highestBlock: 2}
	checker := &Checker{mockClient, 1, 600 * time.Millisecond}

	res := checker.CheckNode(3, time.Second)
	assert.False(t, res.Success)

	require.NotNil(t, res.Error)
	assert.Equal(t, "sync process failed: number of attempts exceeded", res.Error.Error())
}

func TestCheckNodeSuccess(t *testing.T) {
	mockClient := &MockClient{connNum: 3, currentBlock: 2, highestBlock: 2}
	checker := &Checker{mockClient, 1, 600 * time.Millisecond}

	res := checker.CheckNode(3, time.Second)
	assert.True(t, res.Success)
	assert.Nil(t, res.Error)
	assert.Equal(t, uint64(2), res.CurrentBlock)
	assert.NotNil(t, res.Duration)
	assert.NotNil(t, res.SyncedAt)
	assert.NotNil(t, res.StartedAt)
}

type MockClient struct {
	connNum      int
	currentBlock uint64
	highestBlock uint64

	networkConnectionErr  error
	blockchainProgressErr error
}

func (c *MockClient) NetworkConnections() (*api.Connections, error) {
	if c.networkConnectionErr != nil {
		return nil, c.networkConnectionErr
	}

	return &api.Connections{Connections: make([]readable.Connection, c.connNum)}, nil
}

func (c *MockClient) BlockchainProgress() (*readable.BlockchainProgress, error) {
	if c.blockchainProgressErr != nil {
		return nil, c.blockchainProgressErr
	}

	return &readable.BlockchainProgress{Current: c.currentBlock, Highest: c.highestBlock}, nil
}
